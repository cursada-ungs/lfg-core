package interfaces;

import java.util.Collection;
import java.util.List;

import core.Post;

public interface PostProvider {
    Collection<Post> getPost(List<String> keywords);
}