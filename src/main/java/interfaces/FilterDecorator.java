package interfaces;

import java.util.Collection;
import java.util.List;

import core.Post;

public interface FilterDecorator{
	Collection<Post> find(List<String> keywords);
}
