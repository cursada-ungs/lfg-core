package interfaces;

import java.util.*;

import core.Post;

public interface BasePostFinder{
	Collection<Post> filter(List<String> keywords);
	String getProviderName();
}