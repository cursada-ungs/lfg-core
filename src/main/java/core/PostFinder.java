package core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import builders.BasicFilterBuilder;
import interfaces.BasePostFinder;
import interfaces.FilterDecorator;
import interfaces.PostProvider;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PostFinder implements BasePostFinder{

	private String providerName;
	private FilterDecorator filterDecorator;
	
	public PostFinder(PostProvider postProvider) {
		this.providerName = postProvider.getClass().getName();
		this.filterDecorator = new BasicFilterBuilder().build(postProvider);
	}
	
	@Override
	public Collection<Post> filter(List<String> keywords) {
		Collection<Post> filtered_posts = new ArrayList<Post>();
		List<Pattern> patternList = patternListBuilder(keywords);
		
		for (Post post : this.filterDecorator.find(keywords)) {
			if (!meetsPattern(patternList, post) || filtered_posts.contains(post)) continue;
			filtered_posts.add(post);
		}
		
		return filtered_posts;
	}

	private boolean meetsPattern(List<Pattern> patternList, Post post) {
		boolean meets = false;
		for (int i = 0; i < patternList.size(); i++) {
		    Matcher matcher = patternList.get(i).matcher(post.content);
		    
		    if ( !matcher.find() ) { 
		    	meets = false; 
		    	break;
			}
		    meets = true;
		}
		return meets;
	}

	private List<Pattern> patternListBuilder(List<String> keywords) {
		List<Pattern> patternList = new ArrayList<Pattern>();
        for (int i = 0; i < keywords.size(); i++) {
    		String regexBuilder = "(?i)(\\b" + Pattern.quote(keywords.get(i)) + "\\b)";
    		patternList.add(Pattern.compile(regexBuilder, Pattern.CASE_INSENSITIVE));
        }		
		return patternList;
	}

	@Override
	public String getProviderName() {
		return this.providerName;
	}
	
}
