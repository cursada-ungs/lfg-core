package core;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import interfaces.BasePostFinder;
import interfaces.PostProvider;

@SuppressWarnings("deprecation")
public class LFG extends Observable{
	public Map<String, PostProvider> postProviders;
	public BasePostFinder selectedProvider;
	public Collection<Post> posts;
	
	public LFG(Map<String, PostProvider> postProviders, String defaultProvider){
		this.postProviders = postProviders;
		this.selectedProvider = buildProvider(defaultProvider);
		this.posts = new HashSet<Post>();
	}
	
	public Collection<Post> find(List<String> keywords){
		this.posts = this.selectedProvider.filter(keywords);
		this.notifyObservers();
		return this.posts;
	}
	
	public void switchProvider(String provider) {
		this.selectedProvider = buildProvider(provider);
	}
	
	private BasePostFinder buildProvider(String provider) {
		if (!postProviders.containsKey(provider)){
			return null;
		}
		
		PostProvider post_provider = postProviders.get(provider);
		BasePostFinder postFinder = new PostFinder(post_provider);
		return postFinder;
	}
}
