package finders;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

import interfaces.PostProvider;

public class PostProviderFinder {
	public String directory;
	
	public PostProviderFinder(String url) {
		this.directory = url;
	}
		
	public Map<String, PostProvider> find() throws Exception {
		Map<String, PostProvider> classes = new HashMap<>();

		if(new File(this.directory).isFile()) throw new IllegalArgumentException();
		
		if(this.directory == null || new File(this.directory).listFiles() == null) throw new IOException();

		for (File f : new File(this.directory).listFiles()) {
			String filename = f.getName();
			
			if (!filename.endsWith(".class")) continue;			
			
			String[] filename_splited_bar = filename.replace(".class", "").split("/");
			String filenameWithoutDotClass = filename_splited_bar[filename_splited_bar.length-1];
			
			URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { f.toURI().toURL() });
			Class<?> c = Class.forName(filenameWithoutDotClass, true, classLoader);

			if (!PostProvider.class.isAssignableFrom(c)) continue;
			
			classes.put(filenameWithoutDotClass, (PostProvider) c.getDeclaredConstructor().newInstance());

		}
		return classes;
	}
}
