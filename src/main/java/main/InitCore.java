package main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import core.LFG;
import finders.PostProviderFinder;
import interfaces.PostProvider;

public class InitCore {
	private Map<String, PostProvider> postProviders;
	private String defaultProvider;
	
	public InitCore(String postsPath, String configPath) throws Exception {
		this.postProviders = new PostProviderFinder(postsPath).find();
		this.defaultProvider = getDefaultProvider(configPath);
	}
	
	public LFG init() {
		return new LFG(this.postProviders, this.defaultProvider);
	}
		
	private String getDefaultProvider(String configPath) {
		String defaultProvider = loadConfig(configPath).getProperty("DefaultPostProvider");
		return defaultProvider;
	}

	private Properties loadConfig(String configPath) {
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream(configPath) ); // Ex.: "dir/lfg.properties"
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
}
