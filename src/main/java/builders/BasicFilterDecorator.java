package builders;

import java.util.Collection;
import java.util.List;

import core.Post;
import interfaces.FilterDecorator;
import interfaces.PostProvider;

public class BasicFilterDecorator implements FilterDecorator {

	PostProvider postProvider;
	public BasicFilterDecorator(PostProvider postProvider) {
		this.postProvider = postProvider;
	}

	@Override
	public Collection<Post> find(List<String> keywords) {
		return this.postProvider.getPost(keywords);
	}}
