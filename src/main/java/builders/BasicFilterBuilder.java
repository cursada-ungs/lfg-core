package builders;

import interfaces.FilterDecorator;
import interfaces.PostProvider;

public class BasicFilterBuilder implements FilterDecoratorBuilder{

	@Override
	public FilterDecorator build(PostProvider postProvider) {
		return new BasicFilterDecorator(postProvider);
	}

}
