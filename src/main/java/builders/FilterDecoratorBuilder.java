package builders;

import interfaces.FilterDecorator;
import interfaces.PostProvider;

public interface FilterDecoratorBuilder {
	public FilterDecorator build(PostProvider postProvider);
}
