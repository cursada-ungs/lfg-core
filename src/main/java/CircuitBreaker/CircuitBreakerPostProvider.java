package CircuitBreaker;

import java.util.Calendar;
import java.util.function.Function;

public class CircuitBreakerPostProvider<T,V> {
	private CircuitState state;
	private Integer failed_tries;
	private Integer threshold;
	private Calendar last_fail_time;
	private Function<T,V> func;
	private T params;
	private long retry_time;
	
	public CircuitBreakerPostProvider(Integer threshold, long tiempointento) {
		this.threshold = threshold;
		this.retry_time = tiempointento;
		this.state = CircuitState.CLOSED;
		this.failed_tries = 0;
	}

	public V run() throws Exception {
		update();
		switch(state) {
			case CLOSED:
			case HALF_OPEN:
				try {
					V response = func.apply(params);
					reset();
					return response;
				}catch(Exception e) {
					guardarFalla();
				}
			case OPEN:
				throw new Exception("El servicio no esta disponible");
			default:
				throw new Exception("No se pudo reconocer el estado");
		}
	}
	
	private void update() {
		if (this.failed_tries > this.threshold) {
			
			if (Calendar.getInstance().getTimeInMillis() - this.last_fail_time.getTimeInMillis() > retry_time) {
				this.state = CircuitState.HALF_OPEN;
			}
			else {
				this.state = CircuitState.OPEN;
			}
		}
		else {
			this.state = CircuitState.CLOSED;
		}
	}
	
	private void reset() {
		this.failed_tries = 0;
		this.last_fail_time = null;
		this.state = CircuitState.CLOSED;
	}
	
	private void guardarFalla() {
		this.last_fail_time = Calendar.getInstance();
		this.failed_tries++;	
	}
	
	public void setFunc(Function<T, V> func) {
		this.func = func;
	}

	public void setParams(T params) {
		this.params = params;
	}

}
