package CircuitBreaker;

public enum CircuitState {
	OPEN, CLOSED, HALF_OPEN;
}
