package iteracion1;
/*
 * "Quiero extraer post de Discord"
 * Description:
 * "Quiero buscar publicaciones que coincidan con lo párametros ingresados."
 */

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;

import CircuitBreaker.CircuitBreakerPostProvider;
import core.Post;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;

@TestInstance(Lifecycle.PER_CLASS)
class US4 {	
	Collection<Post> posts;
	ArrayList<Post> postsMock;
	@SuppressWarnings("rawtypes")
	CircuitBreakerPostProvider circuitBreakerCorrectMock;
	@SuppressWarnings("rawtypes")
	CircuitBreakerPostProvider circuitBreakerResetMock;
	@SuppressWarnings("rawtypes")
	CircuitBreakerPostProvider circuitBreakerExceptionMock;

	@BeforeAll
	void beforeClass() throws Exception {
		/* Inicializacion */
		circuitBreakerCorrectMock = Mockito.mock(CircuitBreakerPostProvider.class);
		circuitBreakerResetMock = Mockito.mock(CircuitBreakerPostProvider.class);
		circuitBreakerExceptionMock = Mockito.mock(CircuitBreakerPostProvider.class);
	}
	
	@BeforeEach
	void beforeEach() {
		posts = new ArrayList<Post>();
	}
	
	@SuppressWarnings("unchecked")
	@Test 
    void CA1() throws Exception {	
		posts.add(new Post("Dummy CA1"));
		
    	when(circuitBreakerCorrectMock.run()).thenReturn(posts);
    	postsMock = (ArrayList<Post>) circuitBreakerCorrectMock.run();
    	
        assert( postsMock.size() == 1 );
        assert( postsMock.get(0).content == "Dummy CA1" );
    }
	
	@SuppressWarnings("unchecked")
	@Test 
    void CA2() throws Exception {	
		posts.add(new Post("Dummy CA2"));
		
    	when(circuitBreakerResetMock.run()).thenThrow(Exception.class).thenReturn(posts);
        
        assertThrows( Exception.class, () -> {
        	circuitBreakerResetMock.run();
        });

        postsMock = (ArrayList<Post>) circuitBreakerResetMock.run();

        assert( postsMock.size() == 1 );
        assert( postsMock.get(0).content == "Dummy CA2" );
    }
        
    @Test 
    void CA3() throws Exception {	
    	when(circuitBreakerExceptionMock.run()).thenThrow(Exception.class);
        assertThrows(Exception.class, () -> circuitBreakerExceptionMock.run());
    }
}
