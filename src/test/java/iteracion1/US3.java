package iteracion1;

import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import core.LFG;
import finders.PostProviderFinder;
import interfaces.PostProvider;

/*
 * "Seleccion de PostProvider"
 * Description:
 */

@TestInstance(Lifecycle.PER_CLASS)
class US3 {

	LFG lfg;
	Map<String, PostProvider> postProviders;

	@BeforeAll
	void initialize() throws Exception {
		this.postProviders = new PostProviderFinder("build/resources/test/twoImpl").find();
	}
	
	/*
	 * Tiene un selectedProvider valido y se sobreescribe con el mismo
	 */
    @Test 
    void CA1(){
    	lfg = new LFG(postProviders, "RedditScrapperProvider");
    	lfg.switchProvider("RedditScrapperProvider");
    	
    	assert( lfg.selectedProvider.getProviderName() == "RedditScrapperProvider" );
    }
	
	/*
	 * Tiene un selectedProvider valido y se sobreescribe con otro valido
	 */
    @Test 
    void CA2(){
    	lfg = new LFG(postProviders, "RedditScrapperProvider");
    	lfg.switchProvider("DiscordScrapperProvider");
    	
    	assert( lfg.selectedProvider.getProviderName() == "DiscordScrapperProvider" );
    }
    
	/*
	 * Tiene un selectedProvider nulo y se sobreescribe con uno valido
	 */
    @Test 
    void CA3(){
    	lfg = new LFG(postProviders, null);
    	lfg.switchProvider("RedditScrapperProvider");
		
    	assert( lfg.selectedProvider.getProviderName() == "RedditScrapperProvider" );
    }
	 
	/*
	 * Tiene un selectedProvider invalido y se sobreescribe con uno valido
	 */
    @Test 
    void CA4(){		
    	lfg = new LFG(postProviders, "Provider");
    	lfg.switchProvider("RedditScrapperProvider");
		
    	assert( lfg.selectedProvider.getProviderName() == "RedditScrapperProvider" );
    } 
    
	/*
	 * Tiene un selectedProvider valido y se sobreescribe con uno invalido, devuelve nulo
	 */
    @Test 
    void CA5(){		
    	lfg = new LFG(postProviders, "RedditScrapperProvider");
    	lfg.switchProvider("Provider");
		
    	assert( lfg.selectedProvider == null );
    }
    
}
