package iteration0;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.Map;
import org.junit.jupiter.api.Test;
import finders.PostProviderFinder;
import interfaces.PostProvider;

class US2 {

    /**Cuando se utiliza el directorio DirNoImplementation para buscar scrappers, 
     el resultado esperado debe ser una lista vacía de scrappers.**/
	@Test
	void CA1() throws Exception {
		assert(new PostProviderFinder("build/resources/test/noImpl").find().isEmpty());
	}
	 
	/**Una implementación**/
	@Test
	void CA2() throws Exception {
		assert(new PostProviderFinder("build/resources/test/oneImpl").find().containsKey("RedditScrapperProvider"));
	}
	
	/**Múltiples implementaciones**/
	@Test
	void CA3() throws Exception {
		Map<String, PostProvider> scrappers = new PostProviderFinder("build/resources/test/twoImpl").find();
		assert(scrappers.size() == 2);
		assert( scrappers.containsKey("RedditScrapperProvider"));
		assert( scrappers.containsKey("DiscordScrapperProvider"));
	}
	
	/**El resultado de buscar scrappers en UnknowDir debe ser una excepción de directorio inválido. **/
	@Test
	void CA4() throws Exception {
		assertThrows(IOException.class, 
				() -> {
					new PostProviderFinder("build/resources/test/UnknowDir").find();
				});
	}
	
	/**Ubicación inválida
	 * arroja IllegalArgumentException**/
	@Test
	void CA5() throws Exception {
		assertThrows(IllegalArgumentException.class, 
				() -> {
					new PostProviderFinder("build/resources/test/archivo.txt").find();
				});
	}
	
	/**No es una implementación**/
	@Test
	void CA6() throws Exception {
		assertTrue(new PostProviderFinder("build/resources/test/notAnImpl").find().isEmpty());
	}
	
}
