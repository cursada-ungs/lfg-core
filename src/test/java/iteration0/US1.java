package iteration0;
/*
 * "Busqueda de publicaciones"
 * Description:
 * "Quiero buscar publicaciones que coincidan con lo párametros ingresados."
 */

import core.Post;
import core.PostFinder;
import finders.PostProviderFinder;
import interfaces.BasePostFinder;
import interfaces.PostProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
class US1 {
	Map<String, PostProvider> postProviders;
	BasePostFinder postFilter;	
	List<String> regionFactionKeywords;
	List<String> wrongKeywords;
	List<String> emptyKeywords;

	@BeforeAll
	void beforeClass() throws Exception {
		/* Inicializacion de LFG */
		postProviders = new PostProviderFinder("build/resources/test/twoImpl").find();
		
		/* Inicializacion de Keywords */
		regionFactionKeywords = new ArrayList<String>();
		wrongKeywords = new ArrayList<String>();
		emptyKeywords = new ArrayList<String>();	
	}
	
    @Test 
    void CA1() {
		postFilter = new PostFinder(postProviders.get("RedditScrapperProvider"));
		
		regionFactionKeywords.add("FG");
		regionFactionKeywords.add("Q");
		
		ArrayList<Post> result = (ArrayList<Post>) postFilter.filter(regionFactionKeywords);

		assert( result.size() == 1 );
		//assert( result.get(0).content.equals("RedditScrapperProvider - Se busca jugador para reino Q de facción X de FG") );
    }
    
    @Test 
    void CA2() {
		postFilter = new PostFinder(postProviders.get("RedditScrapperProvider"));
		
		wrongKeywords.add("Error");
		wrongKeywords.add("FracCion");
		
		assert( postFilter.filter(wrongKeywords).size() == 0 );
    }    
    
    @Test 
    void CA3() {
		postFilter = new PostFinder(postProviders.get("RedditScrapperProvider"));
		assert( postFilter.filter(emptyKeywords).size() == 0 );
    }    

    @Test 
    void CA4() {
		postFilter = new PostFinder(postProviders.get("DiscordScrapperProvider"));
		assert( postFilter.filter(regionFactionKeywords).size() == 0 );
    }
}
